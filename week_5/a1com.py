INV_COMM_GUNDAM = "Argument 'ON' or 'OFF' or 'SHOOT' or 'MOVE' is required."
INV_COMM_MEGA = "Argument 'ON' or 'OFF' or 'MORPH' or 'MOVE' is required."
GUNDAM_IS_ON = "Launching"
GUNDAM_IS_OFF = "Backing off"
MEGA_ON = "Power Up"
MEGA_OFF = "Power Down"
GUNDAM_SHOOT = 'Shooting with {}'
MEGA_MORPH = "Morphing into {}"
MEGA_MOVE = "Moving to {}"
CHANGE_CODE = "CHANGE"
GUNDAM_MOVE = "Accelerating booster to {}"
ON_CODE = "ON"
OFF_CODE = "OFF"
MOVE_CODE = "MOVE"
SHOOT_CODE = "SHOOT"
MORPH_CODE = "MORPH"


class RobotRemote(object):
    def __init__(self):
        self._history = ()

    @property
    def history(self):
        return self._history

    def execute(self, command, m=None):
        self._history = self._history + (command, )
        if(type(command) == ShootCommand):
            command.weapon = m
        elif(type(command) == MoveCommand):
            command.direction = m
        elif(type(command) == MorphCommand):
            command.form = m
        command.execute()


class Command(object):
    """The COMMAND interface"""

    def __init__(self, obj):
        self._obj = obj

    def execute(self):
        raise NotImplementedError


class TurnOnCommand(Command):
    """The COMMAND for turning on the light"""

    def execute(self):
        self._obj.turn_on()


class TurnOffCommand(Command):
    """The COMMAND for turning off the light"""

    def execute(self):
        self._obj.turn_off()

class ShootCommand(Command):

    def execute(self):
        self._obj.shoot(self.weapon)


class MoveCommand(Command):

    def execute(self):
        self._obj.move(self.direction)


class MorphCommand(Command):

    def execute(self):
        self._obj.morph(self.form)


class Gundam(object):

    def turn_on(self):
        print(GUNDAM_IS_ON)

    def turn_off(self):
        print(GUNDAM_IS_OFF)

    def shoot(self, weapon):
        print(GUNDAM_SHOOT.format(weapon))

    def move(self, direction):
        print(GUNDAM_MOVE.format(direction))


class Megazord(object):

    def turn_on(self):
        print(MEGA_ON)

    def turn_off(self):
        print(MEGA_OFF)

    def morph(self, form):
        print(MEGA_MORPH.format(form))

    def move(self, direction):
        print(MEGA_MOVE.format(direction))


class GundamCockpitClient(object):
    def __init__(self):
        self._gundam = Gundam()
        self._mega_pit = RobotRemote()

    @property
    def cockpit(self):
        return self._mega_pit

    def do(self, cmd, p=None):
        cmd = cmd.strip().upper()
        if cmd == ON_CODE:
            self._mega_pit.execute(TurnOnCommand(self._gundam))
        elif cmd == OFF_CODE:
            self._mega_pit.execute(TurnOffCommand(self._gundam))
        elif cmd == SHOOT_CODE:
            self._mega_pit.execute(ShootCommand(self._gundam), p)
        elif cmd == MOVE_CODE:
            self._mega_pit.execute(MoveCommand(self._gundam), p)
        else:
            print (INV_COMM_GUNDAM)


class MegazordCockpitClient(object):

    def __init__(self):
        self._mega_pit = RobotRemote()
        self._mega = Megazord()

    @property
    def cockpit(self):
        return self._mega_pit

    def do(self, cmd, p=None):
        cmd = cmd.strip().upper()
        if cmd == ON_CODE:
            self._mega_pit.execute(TurnOnCommand(self._mega))
        elif cmd == OFF_CODE:
            self._mega_pit.execute(TurnOffCommand(self._mega))
        elif cmd == MOVE_CODE:
            self._mega_pit.execute(MoveCommand(self._mega), p)
        elif cmd == MORPH_CODE:
            self._mega_pit.execute(MorphCommand(self._mega), p)
        else:
            print (INV_COMM_MEGA)
