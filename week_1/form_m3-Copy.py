class Form:
    _type = ""
    _input = ""
    _button = ""

    def __init__(self, type, input, button):
        self._type = type
        self._input = input
        self._button = button

    def getType(self):
        return self._type.getType()

    def getInput(self):
        return self._input.getInput()

    def getButton(self):
        return self._button.getButton()

class InquiryType:
    _type = ""

    def __init__(self, type):
        self._type = type

    def getType(self):
        return self._type

class InquiryInput:
    _input = ""

    def __init__(self, input):
        self._input = input

    def getInput(self):
        return self._input

class InquiryButton:
    _button = ""

    def __init__(self, button):
        self._button = button
	
    def getButton(self):
        return self._button

### Mandatory checklist no 3 ###
def create_form(builder):
    builder.create_type()
    builder.create_input()
    builder.create_button()
    return builder.form()

class InquiryFormBuilder:
    """docstring for InquiryFormBuilder"""
    def create_type(self):
        return InquiryType("inquiry")

    def create_input(self):
        return InquiryInput("query")

    def create_button(self):
        return InquiryButton("search")

    def form(self):
        return Form(self.create_type(),self.create_input(),self.create_button())
    
def main():
    ### Mandatory checklist no 3 ###
    inquiryForm = create_form(InquiryFormBuilder())
    print("Form type ", inquiryForm.getType())
    print("Form input ", inquiryForm.getInput())
    print("Form button ", inquiryForm.getButton())

main()
