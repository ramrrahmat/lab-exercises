from week4 import SimpleItem, CompositeItem, make_item, make_composite
pencil = make_item("Pencil", 2.00)
scissor = make_item("Scissor", 4.30)
typex = make_item("Type-x", 5.50)
pencilCase = make_composite("Pencil Case", pencil, scissor, typex)
for item in pencilCase:
    item.print()