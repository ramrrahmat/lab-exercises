Struktur :
pada facadeDemo.py, kelas Car merupakan kelas facade yang memiliki objek dari beberapa perintah seperti ignition, engine, fuel tank dan lain-lain. sehingga, tidak perlu repot memanggil beberapa objek hanya kelas car saja.

purpose :
untuk mempermudah dan mengefisienkian pekerjaan

benefit :
menyerdehanakan client dari kekompleksan sistem yang kecil dan tidak terlihat.

how its implemented :
seperti yang dijelaskna pada struktur.