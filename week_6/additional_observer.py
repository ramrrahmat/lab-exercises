class SubscriberOne:

    def __init__(self, name):
        self.name = name

    def update(self, message):
        print('{} got message "{}"'.format(self.name, message))


class SubscriberTwo:

    def __init__(self, name):
        self.name = name

    def receive(self, message):
        print('{} got message "{}"'.format(self.name, message))


class Publisher:

    def __init__(self):
        self.subscribers = dict()

    def register(self, who, callback=None):
        # TODO Complete Me
        self.subscribers[who] = callback

    def unregister(self, who):
        # TODO Complete Me
        del self.subscribers[who]

    def notify(self, message):
        # TODO Complete Me
        for callback in self.subscribers.values():
            if callback is not None:
                callback(message)

