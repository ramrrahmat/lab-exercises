#	Mandatory 9: Edit IdnCurrRates class to a Singleton class
class IdnCurrRates (object):
	class __IdnCurrRates:
		"""docstring for IdnCurrRates"""
		def __init__(self):
			self.rates = None
		def __str__(self):
			return str(self.rates)

	instance = None
	def __new__(self, rates): # __new__ always a classmethod
		if not IdnCurrRates.instance:
			IdnCurrRates.instance = IdnCurrRates.__IdnCurrRates()
		IdnCurrRates.instance.rates = rates
		return IdnCurrRates.instance
	def __getattr__(self, name):
		return getattr(self.instance, name)
	def __setattr__(self, name):
		return setattr(self.instance, name)

		#def __init__(self,rates):
		#	self.rates = rates
		#def __str__(self):
		#	return str(self.rates)
		
x = IdnCurrRates(10000)
print ('Rates x: '+ str(x))
y = IdnCurrRates(20000)
print ('Rates y: ' + str(y))
z = IdnCurrRates(30000)
print ('Rates z: ' + str(z))
print ('X and Y condition should be 30000')
print ('Rates x: ' + str(x))
print ('Rates y: ' + str(y))
